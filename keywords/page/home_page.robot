*** Settings ***
Resource    ${CURDIR}/../../imports/setting.robot

*** Keywords ***
Goto home page
    SeleniumLibrary.Open Browser    ${url}    ${browser}
    SeleniumLibrary.Maximize Browser Window

Input email to Enter email box
    [Arguments]     ${email_input}
    SeleniumLibrary.Wait Until Element Is Visible    ${input_email_button}    ${LOCAL_TIME}
    SeleniumLibrary.Input Text      ${input_email_button}   ${email_input}

Click Start Free Trial button
    SeleniumLibrary.Wait Until Element Is Visible    ${input_submit_button}    ${LOCAL_TIME}
    SeleniumLibrary.Click Element      ${input_submit_button}
*** Settings ***
Resource    ${CURDIR}/../../imports/setting.robot

*** Keywords ***
Verify Signup page is loaded
    SeleniumLibrary.Wait Until Element Is Visible    ${header_text}    ${LOCAL_TIME}
    SeleniumLibrary.Element Should Be Visible   ${header_text}
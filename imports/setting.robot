*** Settings ***
Library    SeleniumLibrary
Library    Collections
Library    String

Resource    ${CURDIR}/../keywords/common/common_keywords.robot

Resource    ${CURDIR}/../keywords/page/home_page.robot
Resource    ${CURDIR}/../keywords/page/signup_page.robot

Resource    ${CURDIR}/../keywords/locator/login_page.robot
Resource    ${CURDIR}/../keywords/locator/signup_page.robot

Resource    ${CURDIR}/../testdata/variable.robot

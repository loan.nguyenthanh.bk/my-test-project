*** Settings ***
Resource    ${CURDIR} /../imports/setting.robot

*** Test Cases ***
Verify that user can input correct format email success
    [Tags]     TC_03
    [Documentation]
    [Arguments]     ${email_input}=${EMAIL}
    home_page.Goto home page
    home_page.Input email to Enter email box    ${email_input}
    home_page.Click Start Free Trial button
    signup_page.Verify Signup page is loaded